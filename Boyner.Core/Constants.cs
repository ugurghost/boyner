﻿namespace Boyner.Core
{
    public class Constants
    {
        public class StatusMessages
        {
            public const string RequestSuccess = "OK";
            public const string RequestError = "NOTOK";

        }

        public class RabbitMQ
        {
            public const string ProductInsertExchange = "ProdInsertExch";
            public const string ProductInsertMongoQueueName = "ProdInsert.Mongo";
            public const string ProductInsertElasticQueueName = "ProdInsert.Elastic";
        }

        public enum ConsumerSource {
            MongoDB,
            ElasticSearch
        }
    }
}
