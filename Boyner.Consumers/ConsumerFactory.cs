﻿using System;
using static Boyner.Core.Constants;

namespace Boyner.Consumers
{
    public class ConsumerFactory
    {
        public static ConsumerBase ReceiveDataFromRMQ(ConsumerSource channel)
        {
            if (channel ==ConsumerSource.MongoDB)
            {
                return new ProductMongoDBConsumer();
            }
            else if (channel == ConsumerSource.ElasticSearch)
            {
                return new ProductElasticConsumer();
            }
            else
                throw new Exception("Wrong Source for RabbitMQ");
        }
    }
}
