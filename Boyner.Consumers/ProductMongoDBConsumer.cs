﻿using Boyner.Core;
using Boyner.Domain;
using Boyner.LogService;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Boyner.Consumers
{
  public  class ProductMongoDBConsumer : ConsumerBase
    {
        private readonly IConnection _connection;
        private bool _isSubscriptionActive;
        private IModel _channel;
        IProductRepository _productRepository;

        public ProductMongoDBConsumer()
        {
 
        }

        public override void ReceiveData()
        {
            Task.Factory.StartNew(() =>
            {
                using (_channel = _connection.CreateModel())
                {
                    ConfigureQueue(_channel);
                    var consumer = new EventingBasicConsumer(_channel);
                    consumer.Received += Consumer_Received;

                    _channel.BasicConsume(queue: Constants.RabbitMQ.ProductInsertMongoQueueName, autoAck: false, consumer: consumer);

                    while (_isSubscriptionActive)
                    {
                        Thread.Sleep(100);
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void Consumer_Received(object sender, BasicDeliverEventArgs eventArguments)
        {
            try
            {
                Log.Write("Consumer MongoDB için çalışmaya başladı");
                var content = Encoding.UTF8.GetString(eventArguments.Body);
                var receivedData = JsonConvert.DeserializeObject(content);

                //Add to MongoDB
                _productRepository.InsertToMongoDB(receivedData);

                _channel.BasicAck(deliveryTag: eventArguments.DeliveryTag, multiple: false);
            }
            catch (Exception ex)
            {
                Log.Write("Consumer MongoDB için çalışırken hata aldı : " + ex.CreateExceptionString());
                _channel.BasicNack(deliveryTag: eventArguments.DeliveryTag, multiple: false, requeue: false);
            }
        }

        private void ConfigureQueue(IModel channel)
        {
            channel.CreateBasicProperties();
            channel.ExchangeDeclare(Constants.RabbitMQ.ProductInsertExchange, ExchangeType.Fanout, durable: true);
            channel.QueueDeclare(Constants.RabbitMQ.ProductInsertMongoQueueName, durable: true, exclusive: false, autoDelete: false);
            channel.QueueBind(Constants.RabbitMQ.ProductInsertMongoQueueName, Constants.RabbitMQ.ProductInsertExchange, "rKey");
        }
    }
}
