﻿using System;

namespace Boyner.Consumers
{
    public abstract class ConsumerBase
    {
        public abstract void ReceiveData();
    }
}
