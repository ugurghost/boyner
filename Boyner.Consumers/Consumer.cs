﻿using System;
using System.Collections.Generic;
using System.Text;
using static Boyner.Core.Constants;

namespace Boyner.Consumers
{
    public class Consumer
    {
        public static void ReceiveData(ConsumerSource channel)
        {
            ConsumerBase cb = ConsumerFactory.ReceiveDataFromRMQ(channel);
            cb.ReceiveData();
        }
    }
}
