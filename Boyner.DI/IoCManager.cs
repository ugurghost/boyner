﻿using Boyner.Business;
using Boyner.Domain;
using Boyner.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nest;
using System;

namespace Boyner.DI
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDIMOdule(this IServiceCollection services, IConfiguration config)
        {
            var mongoConnection = config.GetConnectionString("MongoConnection");
            var rabbitConnection = config.GetConnectionString("RabbitConnection");
            var elasticSearchSettings = config.GetSection("Elasticsearch");
            var elasticSearchUrl = elasticSearchSettings["Url"];
            var elasticSearchIndex = elasticSearchSettings["ProductIndex"];

            #region Repository Registeration
            services.AddSingleton<IProductMongoRepository>(x => new ProductMongoRepository(mongoConnection))
                    .AddSingleton<IRabbitMQRepository>(x => new RabbitMQRepository(rabbitConnection))
                    .AddSingleton<IElasticRepository, ElasticRepository>()
                    .AddSingleton<IProductRepository, ProductRepository>();

            #endregion

            #region Elasticsearch

            services.AddScoped<IElasticClient>(s => new ElasticClient(
                 new ConnectionSettings(new Uri(elasticSearchUrl))
                     .DefaultIndex(elasticSearchIndex)
                     .DefaultMappingFor<Product>(m => m.IndexName(elasticSearchIndex))));

            #endregion

            services.AddSingleton<ILoggerFactory, LoggerFactory>()
                    .AddSingleton(typeof(ILogger<>), typeof(Logger<>));

            #region Business Registeration
            services.AddSingleton<IProductBusiness, ProductBusiness>();
            #endregion

            return services;
        }
    }
}
