﻿namespace Boyner.Domain
{
    public enum ResponseStatus : int
    {
        Success = 1,
        Error = 2,
        UnAuthorized = 3
    }
}
