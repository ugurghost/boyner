﻿using Boyner.Core;
using Newtonsoft.Json;
using System;

namespace Boyner.Domain
{
    public class ServiceResponse<T>
    {
        public ServiceResponse()
        {
            Message = new Message();
            ResultObject = default(T);
        }

        [JsonProperty("reo")]
        public T ResultObject { get; set; }

        [JsonProperty("msg")]
        public Message Message { get; set; }

        [JsonProperty("ed")]
        public string ErrorDetail { get; set; }

        [JsonProperty("st")]
        public ResponseStatus Status { get; set; }

        public void SetSuccess()
        {
            Status = ResponseStatus.Success;
            Message.Status = MessageStatus.Succeess;
            Message.Description = Constants.StatusMessages.RequestSuccess;
        }

        public void SetFailed(Exception ex)
        {
            Status = ResponseStatus.Error;
            Message.Status = MessageStatus.Error;
            Message.Description = Constants.StatusMessages.RequestError;
            ErrorDetail = ex.CreateExceptionString();
        }
    }
}
