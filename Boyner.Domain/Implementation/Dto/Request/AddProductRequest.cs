﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Boyner.Domain
{
    public class AddProductRequest
    {
        [JsonProperty("nm")]
        public string Name { get; set; }
        [JsonProperty("dts")]
        public string Details { get; set; }
        [JsonProperty("prc")]
        public double Price { get; set; }
        [JsonProperty("brd")]
        public string Brand { get; set; }
        [JsonProperty("cats")]
        public List<string> Categories { get; set; }
    }
}
