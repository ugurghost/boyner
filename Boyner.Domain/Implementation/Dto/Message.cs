﻿using Newtonsoft.Json;

namespace Boyner.Domain
{
    public class Message
    {
        [JsonProperty("d")]
        public string Description { get; set; }
        [JsonProperty("st")]
        public MessageStatus Status { get; set; }
    }
}
