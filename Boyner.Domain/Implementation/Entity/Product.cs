﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Boyner.Domain
{
    public class Product : EntityBase
    {
        [JsonProperty("nm")]
        [BsonElement("nm")]
        public string Name { get; set; }
        [JsonProperty("dts")]
        [BsonElement("dts")]
        public string Details { get; set; }
        [JsonProperty("prc")]
        [BsonElement("prc")]
        public double Price { get; set; }
        [JsonProperty("brd")]
        [BsonElement("brd")]
        public string Brand { get; set; }
        [JsonProperty("cats")]
        [BsonElement("cats")]
        public List<string> Categories { get; set; }
    }
}
