﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace Boyner.Domain
{
    public class EntityBase : IEntityBase, IQueueableData 
    {
        public EntityBase()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }


        [JsonProperty("Id")]
        [BsonId]
        [BsonElement("Id", Order = 0)]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        #region CreateDate
        private DateTime createDate;

        [JsonProperty("cd")]
        [BsonElement("cd")]
        public DateTime CreateDate
        {
            get
            {
                if (createDate == null || createDate == DateTime.MinValue)
                {
                    createDate = DateTime.UtcNow;
                }
                return createDate;
            }
            set { createDate = value.ToUniversalTime(); }
        } 
        #endregion
    }
}