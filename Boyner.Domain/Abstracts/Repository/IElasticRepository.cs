﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boyner.Domain
{
    public interface IElasticRepository
    {
        void IndexDocument<T>(T document) where T : class;

        IEnumerable<T> Search<T>(string query) where T : class;
    }
}
