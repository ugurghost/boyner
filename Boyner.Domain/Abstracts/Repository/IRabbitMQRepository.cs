﻿using Boyner.Domain;
using EasyNetQ;
using System;

namespace Boyner.Domain
{
    public interface IRabbitMQRepository
    {
        void PublishToQueue<T>(string queueName, T qObj) where T : class;
        void PublishRMQ<T>(string queueName, T qObj, IBus rabbitBus) where T : class;
        T ReadFromMQ<T>(string queueName) where T : class;
        void DeleteQueue(string queueName);
        void DeclareQueue(string queueName);
        //IDisposable CreateConsumer(string queueName, Action<IMessage<IQueueableData>, MessageReceivedInfo> consumerHandler);
        IAdvancedBus RabbitAdvancedBus { get; set; }
        void PublishToProductInsertExchange<T>(T content) where T : class;

    }
}
