﻿using System.Collections.Generic;

namespace Boyner.Domain
{
    public interface IProductRepository
    {
        Product GetProductById(string id);
        IEnumerable<Product> GetProducts(string queryValue);
        Product Insert(AddProductRequest request);
        bool InsertToMongoDB(object data);
        void InsertToElasticSearch(dynamic data);
    }
}
