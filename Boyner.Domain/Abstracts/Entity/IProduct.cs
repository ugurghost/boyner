﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Boyner.Domain
{
    public interface IProduct : IEntityBase
    {
        [JsonProperty("nm")]
        [BsonElement("nm")]
        string Name { get; set; }
        [JsonProperty("dts")]
        [BsonElement("dts")]
        string Details { get; set; }
        [JsonProperty("prc")]
        [BsonElement("prc")]
        double Price { get; set; }
        [JsonProperty("brd")]
        [BsonElement("brd")]
        string Brand { get; set; }
        [JsonProperty("cats")]
        [BsonElement("cats")]
        List<string> Categories { get; set; }
    }
}
