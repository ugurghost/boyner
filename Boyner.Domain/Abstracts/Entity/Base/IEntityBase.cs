﻿using System;

namespace Boyner.Domain
{
    public interface IEntityBase
    {
        string Id { get; set; }

        DateTime CreateDate { get; set; }
    }
}