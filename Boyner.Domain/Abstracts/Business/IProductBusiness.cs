﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Boyner.Domain
{
    public interface IProductBusiness
    {
        Task<ServiceResponse<Product>> Insert(AddProductRequest request);

        Task<ServiceResponse<Product>> GetProductById(string id);

        Task<ServiceResponse<IEnumerable<Product>>> GetProducts(string queryValue);
    }
}
