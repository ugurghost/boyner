﻿using Boyner.Logger;
using Microsoft.Extensions.Configuration;
using System;

namespace Boyner.LogManager
{
    public class LogFactory
    {
        IConfiguration _config;
        public LogFactory(IConfiguration config)
        {
            _config = config;
            LogDestination = _config.GetSection("LogDestination").Value.ToString();
        }
        private static string LogDestination;
        
        public static LogBase GetLogger()
        {
            if (LogDestination == "db")
            {
                return new DbLogger();
            }
            else if (LogDestination == "event")
            {
                return new EventLogger();
            }
            else
            {
                //if you can't find anything insert the file U.A
                return new TextLogger();
            }
        }
    }
}
