﻿using Boyner.Logger;
using System;
using System.Collections.Generic;
using System.Text;

namespace Boyner.LogManager
{
    public static class Logger
    {
        public static void Write(string msg)
        {
            LogBase lb = LogFactory.GetLogger();
            lb.WriteLog(msg);
        }
    }
}
