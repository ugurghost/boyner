﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boyner.Logger
{
    public abstract class LogBase
    {
        //template method
        public void WriteLog(string msg)
        {
            Log(msg);
        }

        protected abstract void Log(string msg);
    }
}
