﻿using Boyner.Logger;

using System;

namespace Boyner.LogService
{
    public static class Log
    {
        public static void Write(string msg)
        {
            LogManager.Logger.Write(msg);
        }
    }
}
