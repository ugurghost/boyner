﻿using Boyner.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Boyner.Business
{
    public class ProductBusiness : IProductBusiness
    {
        public ProductBusiness(IProductRepository _productRepository, IRabbitMQRepository _rabbitMQRepository)
        {
            productRepository = _productRepository;
        }
        IProductRepository productRepository;
        public async Task<ServiceResponse<IEnumerable<Product>>> GetProducts(string queryValue)
        {
            return await Task.FromResult(GetBySearchValue(queryValue));
        }

        private ServiceResponse<IEnumerable<Product>> GetBySearchValue(string queryValue)
        {
            ServiceResponse<IEnumerable<Product>> result = new ServiceResponse<IEnumerable<Product>>();
            try
            {
                result.ResultObject = productRepository.GetProducts(queryValue);

                result.SetSuccess();
            }
            catch (Exception ex)
            {
                result.SetFailed(ex);
            }
            return result;
        }

        public async Task<ServiceResponse<Product>> GetProductById(string id)
        {
            return await Task.FromResult(GetById(id));
        }

        private ServiceResponse<Product> GetById(string id)
        {
            ServiceResponse<Product> result = new ServiceResponse<Product>();
            try
            {
                result.ResultObject = productRepository.GetProductById(id);

                result.SetSuccess();
            }
            catch (Exception ex)
            {
                result.SetFailed(ex);
            }
            return result;
        }

        /// <summary>
        /// RabbitPublish
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ServiceResponse<Product>> Insert(AddProductRequest request)
        {
            ServiceResponse<Product> result = new ServiceResponse<Product>();
            try
            {
                result.ResultObject = productRepository.Insert(request);

                result.SetSuccess();
            }
            catch (Exception ex)
            {
                result.SetFailed(ex);
            }
            return result;
        }
    }
}
