﻿using Boyner.Consumers;
using Boyner.Core;
using Boyner.DI;
using Boyner.LogService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using static Boyner.Core.Constants;

namespace Boyner.MongoInserter
{
    class Program
    {
        public static IConfiguration Config { get; set; }
        public static IServiceCollection Service { get; set; }
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", false, true);

            Config = builder.Build();

            var services = ServiceCollectionExtension.AddDIMOdule(Service, Config);
            services.BuildServiceProvider();

            Log.Write("Start");
            try
            {
                Consumer.ReceiveData(ConsumerSource.MongoDB);
            }
            catch (Exception ex)
            {
                string err = string.Format("Error ! Consumer has a problem Error Details : {0}", ex.CreateExceptionString());
                Log.Write(err);
            }

            Log.Write("Finish");
        }
    }
}
