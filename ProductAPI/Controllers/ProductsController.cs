﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Boyner.Domain;

namespace ProductAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductBusiness productBusiness;

        public ProductsController(IProductBusiness _productBusiness)
        {
            productBusiness = _productBusiness;
        }

        // GET api/values
        [HttpGet("{id}")]
        public async Task<ServiceResponse<Product>> Get(int id)
        {
            return await productBusiness.GetProductById(id.ToString());
        }

        // GET api/products?query=123
        [HttpGet]
        public async Task<ServiceResponse<IEnumerable<Product>>> Get([FromQuery]string query)
        {
            return await productBusiness.GetProducts(query);
        }

        // POST api/products
        [HttpPost]
        public async Task<ServiceResponse<Product>> Post([FromBody]AddProductRequest productRequest)
        { 
            return await productBusiness.Insert(productRequest);
        }

    }}
