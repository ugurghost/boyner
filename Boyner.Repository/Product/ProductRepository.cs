﻿using Boyner.Domain;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boyner.Repository
{
    public class ProductRepository : IProductRepository

    {
        public ProductRepository(IProductMongoRepository _mongoRepository, IElasticRepository _elasticRepository, IRabbitMQRepository _rabbitMQRepository)
        {
            mongoRepository = _mongoRepository;
            elasticRepository = _elasticRepository;
        }

        IProductMongoRepository mongoRepository;
        IElasticRepository elasticRepository;
        IRabbitMQRepository rabbitMQRepository;
        IElasticClient _client;

        Product IProductRepository.GetProductById(string id)
        {
            return mongoRepository.Find(x => x.Id.Equals(id)).FirstOrDefault();
        }

        public bool InsertToMongoDB(object data)
        {
            Product product = Newtonsoft.Json.JsonConvert.DeserializeObject<Product>(data.ToString());
            return mongoRepository.Insert(product);
        }

        public void InsertToElasticSearch(dynamic data)
        {
            Product product = Newtonsoft.Json.JsonConvert.DeserializeObject<Product>(data.ToString());
             elasticRepository.IndexDocument(product);
        }

        IEnumerable<Product> IProductRepository.GetProducts(string queryValue)
        {
            return elasticRepository.Search<Product>(queryValue).ToList();
        }

        public Product Insert(AddProductRequest request)
        {
            Product p = new Product
            {
                Brand = request.Brand,
                Categories = request.Categories,
                Details = request.Details,
                Name = request.Name,
                Price = request.Price
            };

            rabbitMQRepository.PublishToProductInsertExchange(p);

            return p;
        }
    }
}
