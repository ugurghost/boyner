﻿using Boyner.Domain;

namespace Boyner.Repository
{
    public class ProductMongoRepository : MongoRepository<Product>, IProductMongoRepository
    {
        public ProductMongoRepository(string connectionString) : base(connectionString, "Product")
        {

        }

    }
}
