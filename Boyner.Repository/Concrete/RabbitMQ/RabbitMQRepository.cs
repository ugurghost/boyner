﻿using Boyner.Core;
using Boyner.Domain;
using EasyNetQ;
using EasyNetQ.Topology;
using System;


namespace Boyner.Repository
{
    public class RabbitMQRepository : IRabbitMQRepository
    {

        public RabbitMQRepository(string connectionString)
        {
            RabbitMqUri = connectionString;
            RabbitAdvancedBus = RabbitHutch.CreateBus(RabbitMqUri).Advanced;

        }
        private string RabbitMqUri;

        public IAdvancedBus RabbitAdvancedBus { get; set; }

        public void PublishToQueue<T>(string queueName, T qObj) where T : class
        {
            using (var rabbitBus = RabbitHutch.CreateBus(RabbitMqUri))
            {
                // Bunu silmemek lazım ki yoksa queue yaratılsın...
                rabbitBus.Advanced.QueueDeclare(queueName);

                PublishRMQ(queueName, qObj, rabbitBus);
            }
        }

        public void PublishRMQ<T>(string queueName, T qObj, IBus rabbitBus) where T : class
        {
            // Publish
            var msg = new Message<T>(qObj);
            //1 = Non-Persistant
            //2 = Persistant
            msg.Properties.DeliveryMode = 2;

            rabbitBus.Advanced.Publish(Exchange.GetDefault(), queueName, false, msg);
        }

        public void PublishToProductInsertExchange<T>(T content) where T : class
        {
            try
            {

                IExchange exchange = RabbitAdvancedBus.ExchangeDeclare(Constants.RabbitMQ.ProductInsertExchange, ExchangeType.Fanout, true);

                IQueue elasticQueue = RabbitAdvancedBus.QueueDeclare(Constants.RabbitMQ.ProductInsertElasticQueueName, durable: true, exclusive: false, autoDelete: false);
                RabbitAdvancedBus.Bind(exchange, elasticQueue, "rKey");

                IQueue mongoQueue = RabbitAdvancedBus.QueueDeclare(Constants.RabbitMQ.ProductInsertMongoQueueName, durable: true, exclusive: false, autoDelete: false);
                RabbitAdvancedBus.Bind(exchange, mongoQueue, "rKey");

                PublishToQueue(Constants.RabbitMQ.ProductInsertExchange, content);

            }
            catch (Exception e)
            {
                //todo: some pretty but unhappy loggig will come here (with e.Message).
               // throw new MessagePublishException(e.Message);
            }
        }

        public T ReadFromMQ<T>(string queueName) where T : class
        {
            T message = null;

            using (var rabbitBus = RabbitHutch.CreateBus(RabbitMqUri))
            {
                // Bunu silmemek lazım ki yoksa queue yaratılsın...
                var queue = rabbitBus.Advanced.QueueDeclare(queueName);

                var res = rabbitBus.Advanced.Get<T>(queue);

                if (res.MessageAvailable)
                {
                    message = res.Message.Body;
                }
            }

            return message;
        }

        public void DeleteQueue(string queueName)
        {
            using (IBus rabbitBus = RabbitHutch.CreateBus(RabbitMqUri))
            {
                rabbitBus.Advanced.QueueDelete(rabbitBus.Advanced.QueueDeclare(queueName));
            }
        }

        public void DeclareQueue(string queueName)
        {
            using (IBus rabbitBus = RabbitHutch.CreateBus(RabbitMqUri))
            {
                rabbitBus.Advanced.QueueDeclare(queueName);
            }
        }



        //public IDisposable CreateConsumer(string queueName, Action<IMessage<IQueueableData>, MessageReceivedInfo> consumerHandler)
        //{
        //    IQueue consumerQueue = RabbitAdvancedBus.QueueDeclare(queueName);

        //    return RabbitAdvancedBus.Consume(consumerQueue, consumerHandler);
        //}

    }
}
