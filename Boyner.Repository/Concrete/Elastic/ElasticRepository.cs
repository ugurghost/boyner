﻿using System.Collections.Generic;
using Boyner.Domain;
using Nest;
using System.Linq;
using Newtonsoft.Json;

namespace Boyner.Repository
{
    public class ElasticRepository : IElasticRepository
    {
        private readonly IElasticClient _client;
        public ElasticRepository(IElasticClient client)
        {
            _client = client;

        }

        public void IndexDocument<T>(T document) where T : class
        {
            var index = _client.IndexDocument(document);
            if (index.OriginalException != null)
            {
                //TODO : throw exceptin
            }

        }

        public IEnumerable<T> Search<T>(string query) where T : class
        {
            var searchDescriptor = new SearchDescriptor<dynamic>()
                  .AllTypes()
                  .Query(qry => qry
                      .Bool(b => b
                          .Must(m => m
                              .QueryString(qs => qs
                                  .DefaultField("_all")
                                  .Query(query)))));

            var searchResult = _client.Search<dynamic>(searchDescriptor).Documents.ToList();

            return searchResult.Select(x => (T)JsonConvert.DeserializeObject<T>(x.ToString())).ToList();
        }
    }
}
